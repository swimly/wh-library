/**
 * Created by 97974 on 2016/5/5.
 */
(function($) {
    $.fn.extend({
        //首页banner插件
        Tab: function(options) {
            //设置默认值
            var defaults={
                handle:'.content-nav',
                tab:'.tab',
                tabWrap:'.tab-wrap',
                tabItem:'.tab-item',
                next:'.next',
                prev:'.prev',
                index:0
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var count=$(o.handle).children().length;
                var wid=null;
                var iwid=$(o.tab).width();
                var idx=o.index;
                for(var i=0;i<count;i++){
                    $(o.tabItem).eq(i).width(iwid)
                    wid+=$(o.tabItem).width();
                }
                function change(i){
                    $(o.tabWrap).css('left',-iwid*idx);
                    $(o.handle).children().eq(i).addClass("active").siblings().removeClass("active");
                }
                $(o.tabWrap).width(iwid*count);
                $(".tablew").height($(o.tab).height()-20)
                change(idx);
                function clickChange(i){
                    if(i>=0 && idx<count-1){
                        idx++;
                    }
                    else if(i<0 && idx>0){
                        idx--;
                    }
                    change(idx)
                }
                $(o.handle).children().click(function(){
                    idx=$(this).index();
                    change(idx);
                })
                $(o.next).click(function(){
                    clickChange(0);
                })
                $(o.prev).click(function(){
                    clickChange(-1);
                })
            });
        },
        Scroll:function(){
            //设置默认值
            var defaults={
                dom:'.txtMarquee-top .bd',
                wrap:".infoList",
                pause:".scrollBtn",
                auto:false
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var obj=$(o.dom);
                var timer=null;
                var ul=obj.find(o.wrap).html(obj.find(o.wrap).html()+obj.find(o.wrap).html())
                $(o.wrap).css("position","relative");
                var i=0;
                function move(){
                    var tops=$(o.wrap).scrollTop()-i;
                    i++
                    if(i>=$(o.dom).height()){
                        i=0;
                    }
                    $(o.wrap).css({"top":tops})
                }
                $(o.pause).click(function(e){
                    e.stopPropagation();
                    o.auto=!o.auto;
                    if(!o.auto){
                        clearInterval(timer)
                        $(o.pause).removeClass("active")
                    }else{
                        clearInterval(timer)
                        timer=setInterval(move,50)
                        $(o.pause).addClass("active")
                    }
                })
            });
        },
        Banner: function(options) {
            //设置默认值
            var defaults={
                index:0,
                speed:100,
                delay:4000,
                effect:"linner",
                item:".banner-item",
                phoneImg:["img/banner.jpg","img/banner.jpg","img/banner.jpg","img/banner.jpg","img/banner.jpg"],
                tabBg:"img/banner.jpg"//如果背景不变的话，直接填字符串，如果多张背景切换，则填数组
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var obj=$(this);
                var items=$(o.item,obj);
                var phone=$(window).width()<=600?true:false;
                function init(){
                    if(phone){
                        items.width($(window).width())
                        items.each(function(i,ele){
                            $(this).html('<img src='+o.phoneImg[i]+'>')
                        })
                        o.delay=3000;
                        obj.width(items.width()*items.length).addClass("phone");
                    }else{
                        if(typeof o.tabBg=="string"){
                            if(o.tabBg.search("#")==0){
                                obj.css("background-color",o.tabBg);
                            }else{
                                obj.css("background-image","url("+o.tabBg+")");
                            }
                            items.each(function(index,ele){
                                var itemobj=$(".animated",$(this));
                                itemobj.hide()
                            })
                        }else{
                            items.each(function(index,ele){
                                var bg="url("+o.tabBg[index]+")";
                                $(this).css("background-image",bg);
                                var itemobj=$(".animated",$(this));
                                itemobj.hide()
                            })
                        }
                    }
                }
                init();
                function change(i){
                    if(phone){
                        obj.css("margin-left",-$(window).width()*o.index)
                    }else{
                        items.eq(i).fadeIn(o.speed).siblings().fadeOut(o.speed);
                        var itemc=$(".animated",items.eq(i));
                        var sibitem=$(".animated",items.eq(i).siblings());
                        itemc.each(function(index,ele){
                            var _this=$(this);
                            setTimeout(function(){
                                _this.fadeIn("fast").addClass(_this.attr("entry"))
                            },$(this).attr("delay"))
                            setTimeout(function(){
                                _this.removeClass(_this.attr("entry")).addClass(_this.attr("leave"))
                            },o.delay-_this.attr("delay")-500)
                            setTimeout(function(){
                                _this.removeClass(_this.attr("leave")).hide()
                            },o.delay)
                        })
                    }
                }
                change(o.index)
                setInterval(function(){
                    if(o.index<items.length-1){
                        o.index++;
                    }else{
                        o.index=0;
                    }
                    change(o.index)
                },o.delay)
            });
        },
        Pop:function(options){
            //设置默认值
            var defaults={
                content:'',
                margintop:0,
                effect:"click"
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                $(this).bind(options.effect,function(e){
                    e.stopPropagation();
                    var left=$(this).offset().left;
                    var top=$(this).offset().top
                    var height=e.delegateTarget.clientHeight;
                    var width=e.delegateTarget.clientWidth;
                    $(".pop").remove()
                    $("<div class='pop'>"+options.content+"<span class='popa'></span></div>")
                        .css({
                            position:"fixed",
                            left:left+width/2,
                            top:top+height+options.margintop,
                            zIndex:110
                        })
                        .appendTo($(document.body));
                    var popw=$(".pop").width();
                    var popl=$(".pop").offset().left;
                    if(left+popw/2>$(window).width()){
                        $(".pop").css({
                            left:"auto",
                            right:10
                        })
                    }else{
                        $(".pop").css({
                            left:popl-popw/2
                        })
                    }
                    $(".popa").css({
                        position:"fixed",
                        top:top+height-5,
                        left:left+width/2-10,
                        zIndex:110
                    })
                })
                $(document).bind("click",function(){
                    $(".pop").remove()
                })
            });
        }
    });
})(jQuery);



/*插件调用*/
$("#banner").Banner({tabBg:["img/banner1.jpg","img/banner2.jpg"]})
$("#tab1").Tab();
$("#tab2").Tab({index:0});
$("#tab3").Tab({index:2});
$("#tab4").Tab({index:1});
$("#tab6").Tab({index:0});
$("#weichat").Pop({content:'<img src="img/code.png"/>',margintop:5})
$("#app").Pop({content:'暂无',margintop:5})
/*
var posterTvGrid86804 = new posterTvGrid('posterTvGrid86804',{className: "posterTvGrid"},[
        {"img":"img/slide1.jpg","title":"0","url":""},
        {"img":"img/slide2.jpg","title":"1","url":""},
        {"img":"img/slide3.jpg","title":"2","url":""},
        {"img":"img/slide4.jpg","title":"3","url":""},
        {"img":"img/slide5.jpg","title":"4","url":""},
        {"img":"img/slide6.jpg","title":"5","url":""},
        {"img":"img/slide7.jpg","title":"6","url":""},
        {"img":"img/slide8.jpg","title":"7","url":""},
        {"img":"img/slide9.jpg","title":"8","url":""}
    ]
);*/

$(".index-grid").find("td").click(function(){
    var link=$(this).attr("link");
    if(link){
        location.href=$(this).attr("link")
    }else{
        /*alert("模块正在开发中……")*/
    }
})
$(".txtMarquee-top").Scroll();
/*控制音乐播放*/
var obj=document.getElementById("music")
if(obj){
    $(".musicBtn").click(function(e){
        e.stopPropagation();
        if(obj.paused){
            obj.play();
            $(this).addClass("active")
        }else{
            obj.pause();
            $(this).removeClass("active");
        }
    })
    var musicTimer=setInterval(function(){
        if(obj.ended){
            $(".musicBtn").removeClass("active");
        }
    },10)
}

$(".content-hd h2").click(function(){
    if($(".aside").hasClass("active")){
        $(".aside").removeClass("active");
    }else{
        $(".aside").addClass("active");
    }
})